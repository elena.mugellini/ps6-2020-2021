---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Gamifying a VR phobia therapy app. 
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Fouad Hannoun
mots-clés: [3D scenery creation, game design, Unity, psychology]
langue: [F,E]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.6\textwidth]{img/achievement.png}
\end{center}
```


## Description/Contexte

The goal of this project is to add a gaming feeling to an existing VR app that targets fear of heights and of spiders. We would like to explore many areas like adding difficulty levels, achievements etc...


## Objectifs

1. Explore the latest technologies in the field
2. Come up with gamification ideas
3. Implementation
4. Test the plateform
