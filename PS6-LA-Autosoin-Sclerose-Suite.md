---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Application pour l'autosion de la Sclérose En Plaques
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Leonardo Angelini
proposé par étudiant: Corentin Bompard
mots-clés: [autosoins, Scélorose En Plaques, app, Flutter]
langue: [F,E]
confidentialité: non
suite: oui
---

## Contexte

Dans le cadre du projet de semestre 5, en collaboration avec des étudiants de la Haute Ecole de Santé, une application pour l'autosoin des la sclérose en plaques a été imaginée et conçue. Un premier prototype basique a été dévéloppé en Flutter. Le but de ce projet est d'implementer une première version de cette application qui puisse être testée avec des utilisateurs cible.


## Objectifs
- Analyse des librairies pour la gestion du calendrier er pour la création de graphiques
- Conception et implementation de l'application
- Conception et implementation d'un backend
- Tests utilisateur 

## Contraintes
Suite du projet de semestre 5 en collaboration avec la HEdS
