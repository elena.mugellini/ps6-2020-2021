---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Prédiction et visualisation de l’état physiologique d’un conducteur en temps réel
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Quentin Meteier
mots-clés: [Python, Machine Learning, temps réel, conduite autonome, physiologie]
langue: [F,E]
confidentialité: non
suite: non
---


## Description/Contexte

Le projet AdVitam, mené par l’Institut HumanTech en collaboration avec l’Université de Fribourg et l’He-Arc, vise à comprendre quelles sont les situations dangereuses pour la conduite semi-autonome d’un véhicule et ainsi développer de nouvelles interfaces homme-voiture afin que le conducteur puisse mieux gérer ces situations. En particulier, le but est de mesurer l’état physiologique du conducteur en temps réel afin d’adapter les informations transmises et lui permettre d’être prêt à reprendre le contrôle du véhicule. 
Des modèles de Machine Learning ont été déjà entraînés, sur la base de données physiologiques de plus de 200 personnes récoltées lors de plusieurs expériences sur un simulateur de conduite. Les modèles ont été entraînés en utilisant le framework Scikit-learn en Python. 

Le but de ce projet est de prédire l’état du conducteur en temps réel, en utilisant les modèles déjà entraînés. Pour cela, les données déjà récoltées ou un kit de capteurs permettant de mesurer les signaux physiologiques pourront être utilisés. Un système de visualisation de l’état du conducteur devra aussi être implémentée, afin de visualiser l'état du conducteur à l’aide d’un icone.



## Objectifs

Pendant le projet l’étudiant devra accomplir les tâches suivantes :

1. Se familiariser avec les signaux et capteurs physiologiques [1]
2. Calculer des indicateurs à la base des signaux physiologiques en utilisant la librairie Neurokit [2] en Python
3. Adapter la pipeline existante en Python permettant de prédire l’état du conducteur en temps réel et l’adapter aux nouveaux modèles entraînés
4. Tester la qualité de la prédiction de l’état du conducteur
5. Implémenter la visualisation de l’état du conducteur à l’aide d’une icone
6. Optionnel : Mesurer et optimiser le temps écoulé entre la récolte des signaux physiologiques et la prédiction de l’état du conducteur
