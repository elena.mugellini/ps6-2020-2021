---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Puzzle 3D en Réalité Augmentée
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Marine Capallera
  - Karl Daher
proposé par étudiant: Jonathan Vicente Dias
mots-clés: [Gamification, games, Modelling, Réalité Augmentée, Unity]
langue: [F,E]
confidentialité: non
suite: oui
---


## Description/Contexte
De nos jours, les artistes ont du mal à se faire connaitre, même au sein de leur communauté. En parallèle, les jeux prennent de plus en plus de place dans la vie de tous. Enfants et adultes utilisent de plus en plus d’applications ludiques. Dans le but d’associer l’art et la technologie, ce projet va permettre de concevoir des jeux afin de mettre en avant les travaux d’un artiste local. Le but principal est de créer un générateur de jeux 3D en réalitée augmentée.
Un jeu de puzzle en 3D a déjà été développé au cours du précédent semestre. Le but de ce projet sera d'apporter des améliorations à la solution existance et de rajouter une feature pour pouvoir résoudre le puzzle en Réalitée Augmentée.
Suivant l’avancement du projet, différents types de jeux pourront être implémentés. Par exemple, chercher des indices dans le modèle 3D.  



## Objectifs

Durant ce projet de semestre, l’étudiant devra accomplir les tâches suivantes :

1. Prendre connaissance du projet existant
2. Apporter des améliorations à la solution existante
3. Implémenter la Réalitée Augmentée
4. Réaliser des tests fonctionnels et utilisateurs
5. Concevoir et implémenter d'autres activités 

## Contraintes

1. Utilisation de logiciel Unity
