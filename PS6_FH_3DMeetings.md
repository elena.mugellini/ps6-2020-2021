---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Designing a online 3D virtual meeting area. 
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
mandants: 
  - Département de psychologie UNIFR - Dr. Marius Rubo
professeurs co-superviseurs:
  - Elena Mugellini
  - Fouad Hannoun
mots-clés: [3D scenery creation, game design, Unity]
langue: [F,E]
confidentialité: non
suite: non
---
\begin{center}
\includegraphics[width=0.6\textwidth]{img/3D-space.png}
\end{center}


## Description/Contexte

Online meetings are mostly held using video conferencing tools such as MS Teams or Zoom. This scenery project will explore possibilities to meet and chat in a shared 3D scene. While such an approach does not allow for all of the functionalities of video conferencing (e.g. sharing screens and showing presentations), it may be more comfortable to use in some forms of meetings, help users to identify as a group, communicate non-verbally and allow to split in subgroups in a direct and embodied way. 


## Objectifs

1. Explore the latest technologies in the field
2. Design a 3D scene
3. Add network functionalities to be able to meet online
4. Potentially add video chat
5. Test the plateform
