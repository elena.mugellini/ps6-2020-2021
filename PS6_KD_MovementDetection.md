---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Détection des mouvements à partir d'une vidéo
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Elena Mugellini
  - Marine Capallera
  - Karl Daher

mots-clés: [Squelette, Analyse Vidéo, Traitement d'image, Body Language]
langue: [F,E]
confidentialité: non
suite: oui
---

\begin{center}
\includegraphics[width=0.7\textwidth]{img/danse_libre.png}
\end{center}


## Description/Contexte
Cet été, nous avons réalisé une série de vidéos avec la participation d’une danseuse contemporaine. Son rôle était d’illustrer différentes émotions (joie, colère, tristesse, etc.) au travers de courtes performances. En plus de ces performances, nous avons également filmé l’ombre de la danseuse derrière un écran. Le but de ces vidéos sera de pouvoir reconnaître et classifier automatiquement ces émotions.
Afin de pouvoir reconnaitre les émotions des danseurs, nous avons besoin d’analyser ces vidéos. Pour cela, il est nécessaire d’extraire le squelette pour analyser les mouvements. Deux vidéos de la performance seront fournies : la vue de face et de l’ombre.
Un projet est déja implémenté pour extraire et afficher le squelette sur les vidéos fournies. 
Nous avons besoin maintenant de pouvoir detecter les mouvements, gestes et poses et les comparer avec des gestes en temps réel.

## Objectifs

Durant ce projet de semestre, l’étudiant devra accomplir les tâches suivantes :

 Prendre connaissance du projet existant
 Apporter des améliorations à la solution existante
 Implémenter la détection de mouvement, geste et de pose
 Réaliser des tests fonctionnels et utilisateurs
 Créer et tester un pipeline d’automatisation du processus
