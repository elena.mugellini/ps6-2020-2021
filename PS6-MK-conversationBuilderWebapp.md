---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Conversation Builder Web application
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Mira El Kamali
  - Leonardo Angelini
  - Elena Mugellini

mots-clés: [botfront UI, RASA, conversational agent]
langue: [F,E]
confidentialité: non
suite: non
---


## Description/Contexte

Le but de ce projet est de créer une interface pour gérer les conversations d'agent conversationnel avec RASA. Un framework "botfront" déjà existant https://botfront.io/docs/extending-rasa/ peut être valide. L'idée est de pouvoir ajouter quelques fonctionnalités à cette interface (ex: différentes langues, conversations pour le chatbot et / tangible coach et ensuite l'intégrer avec des intentions et des phrases de notre base de données), et la connecter avec le chatbot existant et tangible coach via Apis.


## Buts

Le projet est divisé en 4 étapes:
1. Analyse des plateformes existantes, en particulier Botfront
2. Testez la plateforme choisie
3. Conception des fonctionnalités à ajouter
4. Intégration avec la base de données et les agents conversationnels
4. Tests de plate-forme
