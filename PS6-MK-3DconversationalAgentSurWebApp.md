---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2020/2021
titre: Agent Conversationnel Vocal en 3D Web application
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Mira El Kamali
  - Leonardo Angelini
  - Elena Mugellini

mots-clés: [speechRecognition, 3D, Progressive Web App, avatar]
langue: [F,E]
confidentialité: non
suite: non
---


## Description/Contexte

L'objectif de ce projet est de créer une plateforme web, également accessible via des appareils mobiles, centrée autour du coach tangible du projet NESTORE. Le coach tangible du projet NESTORE est un objet physique qui a des capacités tangibles et vocales. Ce coach tangible a 2 positions: la position de veille où il peut ne rien dire ou écouter et la position d'éveil, qui peut finalement communiquer avec la voix. Le coach dispose également de 3 lumières qui illustrent les différentes étapes de la conversation: écouter, réfléchir, parler. De plus, il dispose d'autres motifs et couleurs pour les afficher en fonction des émotions.
L'idée est de pouvoir afficher le coach tangible en 3D sur la page web et communiquer avec lui par la voix. Ce dernier pourra reconnaître ce que l'utilisateur vient de dire (Speech API) et lui répondra par une réponse vocale (APIs déjà développé). De plus, l'entraîneur tangible doit afficher les lumières en fonction des étapes qu'il est. Enfin, l'utilisateur pourra changer la position du coach (veille / sommeil) avec son doigt (s'il est au téléphone) ou avec une flèche si l'appareil n'est pas touchable.  

## Objectifs

Le projet se découpe en 6 étapes :
1. Analyse des technologies existantes pour afficher le coach tangible en 3D
2. Conception de l'application 
3. Implémentation du coach, avec la reconnaissance vocale + wakeword
4. Implémentation les motifs des lumieres par rapport a l'état du coach
5. Implémentation le changement de position
6. Tests d'application
